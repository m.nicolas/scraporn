# scraporn-front

Documentation rapide du lancement et utilisation des modules de l'application.

### Installation du projet et ses dépendances
```
yarn install
```

### Compilation du projet en hot-reload pour le developpement
```
yarn serve
```

### Compilation du projet pour la mise en production
```
yarn build
```

### Lancement des test unitaires
```
yarn test:unit
```

### Lancement des test end-to-end
*Nécessite qu'un serveur de developpement soit lancé au préalable*
```
yarn test:e2e
```

### Lancement des tests linter
```
yarn lint
```
