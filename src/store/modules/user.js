const state = {
  userAuth: false,
  userData: null
};

const getters = {
  getUserAuth: state => state.userAuth
};

const actions = {
  userSetter: function({ commit }, payload) {
    return new Promise((resolve, reject) => {
      if (payload) {
        commit("setUser", payload);
        resolve(true);
      } else {
        reject(true);
      }
    });
  }
};

const mutations = {
  setUser: function(state, payload) {
    state.userData = payload;
    state.userAuth = true;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
